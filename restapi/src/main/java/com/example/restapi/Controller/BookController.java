package com.example.restapi.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.restapi.Model.Book;
import com.example.restapi.Service.BookService;

@RestController
public class BookController {
    @Autowired
    private BookService service;

    @CrossOrigin
    @RequestMapping("/api")
    @GetMapping("/book")
    public List<Book> getBook() {
        return service.createBook();
    }

}
