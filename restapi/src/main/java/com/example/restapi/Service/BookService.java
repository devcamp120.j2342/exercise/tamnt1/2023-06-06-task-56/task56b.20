package com.example.restapi.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

import com.example.restapi.Model.Author;
import com.example.restapi.Model.Book;

@Service
public class BookService {

    public List<Book> createBook() {

        ArrayList<Book> bookList = new ArrayList<>();

        ArrayList<Author> authors = new ArrayList<>();

        Author author1 = new Author("Tam", "tam@gmail.com", 'm');
        Author author2 = new Author("Minh", "minh@gmail.com", 'm');
        Author author3 = new Author("Trinh", "trinh@gmail.com", 'f');

        ArrayList<Author> authorList1 = new ArrayList<>();
        authorList1.add(author1);
        ArrayList<Author> authorList2 = new ArrayList<>();
        authorList2.add(author2);
        authorList2.add(author3);
        Book book1 = new Book("Pi of life", authorList1, 20000);
        Book book2 = new Book("Stockholme", authorList2, 20000);
        Book book3 = new Book("Petter pan", authorList2, 20000);
        bookList.addAll(Arrays.asList(book1, book2, book3));
        authors.addAll(Arrays.asList(author1, author2, author3));

        for (Author author : authors) {
            System.out.println(author);
        }

        for (Book book : bookList) {
            System.out.println(book);
        }
    }
}
